#!/bin/bash
docker rm -f tutor-pipeline
mkdir -p "$(tutor plugins printroot)"/
cp plugin/pipeline-plugin.py "$(tutor plugins printroot)"/
tutor plugins enable pipeline-plugin
mkdir -p "$(tutor plugins printroot)"/templates/pipeline-plugin/build/pipeline/
cp "$(tutor config printroot)"/config.yml "$(tutor plugins printroot)"/templates/pipeline-plugin/build/pipeline/
cp service/* "$(tutor plugins printroot)"/templates/pipeline-plugin/build/pipeline/
tutor config save
tutor images build pipeline
tutor local run --name tutor-pipeline -dp 12345:12345/tcp pipeline