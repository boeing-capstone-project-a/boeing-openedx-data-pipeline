# Instructions

1. Clone this repository
1. Make sure Tutor is already running (`tutor local start -d`)
1. Run `init-pipeline.sh`
    - Windows: run `./init-pipeline.sh` in Git Bash
    - Linux/macOS: run `./init-pipeline.sh` in Terminal

JSON data can be fetched from http://localhost:12345/csvToPy.json
