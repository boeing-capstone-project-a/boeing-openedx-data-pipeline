import os
from tutor import hooks

template_folder = os.path.join(os.path.dirname(__file__), "templates")
hooks.Filters.ENV_TEMPLATE_ROOTS.add_item(template_folder)

hooks.Filters.ENV_TEMPLATE_TARGETS.add_item(
    ("pipeline-plugin/build", "plugins")
)

hooks.Filters.IMAGES_BUILD.add_item(
    (
        "pipeline", # same name that will be passed to the `build` command
        ("plugins", "pipeline-plugin", "build", "pipeline"), # path to the Dockerfile folder
        "pipeline:latest", # Docker image tag
        (), # custom build arguments that will be passed to the `docker build` command
    )
)

hooks.Filters.ENV_PATCHES.add_item(
    (
        "local-docker-compose-services",
        """
pipeline:
    image: pipeline:latest
"""
    )
)
